#ifndef SPRITES_H
#define SPRITES_H
#include "raylib.h"
#include "screens_manager/screens.h"

namespace sokoban {
	namespace sprites {

		extern Texture2D playerUp;
		extern Texture2D playerDown;
		extern Texture2D playerLeft;
		extern Texture2D playerRight;
		extern Texture2D ball;
		extern Texture2D hole;
		extern Texture2D grass;
		extern Texture2D darkGrass;
		extern Texture2D soil;
		extern Texture2D darkSoil;
		extern Texture2D spikes;
		extern Texture2D activeSpikes;
		extern Texture2D star;
		extern Texture2D tinyStar;
		extern Texture2D greenButon1;
		extern Texture2D greenButton2;
		extern Texture2D yellowButton1;
		extern Texture2D yellowButton2;
		extern Texture2D pauseButton1;
		extern Texture2D pauseButton2;
		extern Texture2D retryButton1;
		extern Texture2D retryButton2;
		extern Texture2D muteButton1;
		extern Texture2D muteButton2;
		extern Texture2D unMuteButton1;
		extern Texture2D unMuteButton2;

		void init();
		void drawBackClouds();
		void drawFrontClouds();
		void deinit();
	}
}
#endif 