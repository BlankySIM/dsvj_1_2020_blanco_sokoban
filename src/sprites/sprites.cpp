#include "sprites.h"

namespace sokoban {
	namespace sprites {

		const int amountOfClouds = 4;
		Image cloudImage[amountOfClouds];
		Texture2D cloud[amountOfClouds];
		Vector2 cloudPos[amountOfClouds];
		float cloudSpeed[amountOfClouds];
		Image playerUpImage;
		Image playerDownImage;
		Image playerLeftImage;
		Image playerRightImage;
		Image ballImage;
		Image holeImage;
		Image grassImage;
		Image darkGrassImage;
		Image soilImage;
		Image darkSoilImage;
		Image spikesImage;
		Image activeSpikesImage;
		Image starImage;
		Image tinyStarImage;
		Image greenButonImage1;
		Image greenButtonImage2;
		Image yellowButtonImage1;
		Image yellowButtonImage2;
		Image pauseButtonImage1;
		Image pauseButtonImage2;
		Image retryButtonImage1;
		Image retryButtonImage2;
		Image muteButtonImage1;
		Image muteButtonImage2;
		Image unMuteButtonImage1;
		Image unMuteButtonImage2;
		Texture2D playerUp;
		Texture2D playerDown;
		Texture2D playerLeft;
		Texture2D playerRight;
		Texture2D ball;
		Texture2D hole;
		Texture2D grass;
		Texture2D darkGrass;
		Texture2D soil;
		Texture2D darkSoil;
		Texture2D spikes;
		Texture2D activeSpikes;
		Texture2D star;
		Texture2D tinyStar;
		Texture2D greenButon1;
		Texture2D greenButton2;
		Texture2D yellowButton1;
		Texture2D yellowButton2;
		Texture2D pauseButton1;
		Texture2D pauseButton2;
		Texture2D retryButton1;
		Texture2D retryButton2;
		Texture2D muteButton1;
		Texture2D muteButton2;
		Texture2D unMuteButton1;
		Texture2D unMuteButton2;

		void init() {
			cloudSpeed[0] = 70.0f;
			cloudSpeed[1] = 70.0f;
			cloudSpeed[2] = 20.0f;
			cloudSpeed[3] = 20.0f;
			cloudPos[0] = { static_cast<float>(screens::screenWidth - screens::screenWidth / 3),
			static_cast<float>(screens::screenZero + screens::screenHeight/20)};
			cloudPos[1] = { static_cast<float>(screens::screenZero + screens::screenWidth / 4),
			static_cast<float>(screens::screenHeight - screens::screenHeight / 4) };
			cloudPos[2] = { static_cast<float>(screens::screenZero + screens::screenWidth / 30),
			static_cast<float>(screens::screenZero + screens::screenHeight / 20) };
			cloudPos[3] = { static_cast<float>(screens::screenWidth - screens::screenWidth / 3),
			static_cast<float>(screens::screenHeight - screens::screenHeight / 3) };
			playerUpImage = LoadImage("res/character/fox_up.png");
			playerDownImage = LoadImage("res/character/fox_down.png");
			playerLeftImage = LoadImage("res/character/fox_left.png");
			playerRightImage = LoadImage("res/character/fox_right.png");
			ballImage = LoadImage("res/terrain/ball.png");
			holeImage = LoadImage("res/terrain/hole.png");
			grassImage = LoadImage("res/terrain/grass.png");
			darkGrassImage = LoadImage("res/terrain/grass_dark.png");
			soilImage = LoadImage("res/terrain/soil.png");
			darkSoilImage = LoadImage("res/terrain/soil_dark.png");
			spikesImage = LoadImage("res/terrain/spikes.png");
			activeSpikesImage = LoadImage("res/terrain/spikes_active.png");
			cloudImage[0] = LoadImage("res/background/cloud_01.png");
			cloudImage[1] = LoadImage("res/background/cloud_02.png");
			cloudImage[2] = LoadImage("res/background/cloud_03.png");
			cloudImage[3] = LoadImage("res/background/cloud_04.png");
			starImage = LoadImage("res/ui/star.png");
			tinyStarImage = LoadImage("res/ui/star.png");
			greenButonImage1 = LoadImage("res/ui/button_green_01.png");
			greenButtonImage2 = LoadImage("res/ui/button_green_02.png");
			yellowButtonImage1 = LoadImage("res/ui/container_01.png");
			yellowButtonImage2 = LoadImage("res/ui/container_02.png");
			pauseButtonImage1 = LoadImage("res/ui/pause_button_01.png");
			pauseButtonImage2 = LoadImage("res/ui/pause_button_02.png");
			retryButtonImage1 = LoadImage("res/ui/retry_button_01.png");
			retryButtonImage2 = LoadImage("res/ui/retry_button_02.png");
			muteButtonImage1 = LoadImage("res/ui/mute_button_01.png");
			muteButtonImage2 = LoadImage("res/ui/mute_button_02.png");
			unMuteButtonImage1 = LoadImage("res/ui/mute_button_03.png");
			unMuteButtonImage2 = LoadImage("res/ui/mute_button_04.png");
			ImageResize(&playerUpImage, static_cast<int>(screens::screenWidth / 20.94f),
				static_cast<int>(screens::screenHeight / 11.78f));
			ImageResize(&playerDownImage, playerUpImage.width, playerUpImage.height);
			ImageResize(&playerLeftImage, playerUpImage.width, playerUpImage.height);
			ImageResize(&playerRightImage, playerUpImage.width, playerUpImage.height);
			ImageResize(&ballImage, playerUpImage.width, playerUpImage.height);
			ImageResize(&holeImage, static_cast<int>(screens::screenWidth / 14.4f),
				static_cast<int>(screens::screenHeight / 16.2f));
			ImageResize(&grassImage, holeImage.width, holeImage.height);
			ImageResize(&darkGrassImage, holeImage.width, holeImage.height);
			ImageResize(&soilImage, holeImage.width, (holeImage.height/3));
			ImageResize(&darkSoilImage, holeImage.width, (holeImage.height/3));
			ImageResize(&spikesImage, playerUpImage.width, playerUpImage.height);
			ImageResize(&activeSpikesImage, playerUpImage.width, playerUpImage.height);
			ImageResize(&starImage, screens::middleScreenW / 8, screens::screenHeight / 9);
			ImageResize(&tinyStarImage, screens::middleScreenW / 25, static_cast<int>(screens::screenHeight / 28.125f));
			ImageResize(&greenButonImage1, static_cast<int>(screens::screenWidth / 28.44f),
				static_cast<int>(screens::screenHeight / 16.0f));
			ImageResize(&greenButtonImage2, greenButonImage1.width, greenButonImage1.height);
			ImageResize(&pauseButtonImage1, greenButonImage1.width, greenButonImage1.height);
			ImageResize(&pauseButtonImage2, greenButonImage1.width, greenButonImage1.height);
			ImageResize(&retryButtonImage1, greenButonImage1.width, greenButonImage1.height);
			ImageResize(&retryButtonImage2, greenButonImage1.width, greenButonImage1.height);
			ImageResize(&yellowButtonImage1, static_cast<int>(screens::screenWidth / 6.4f), 
				static_cast<int>(screens::screenHeight / 10.30f));
			ImageResize(&yellowButtonImage2, yellowButtonImage1.width, yellowButtonImage1.height);
			ImageResize(&muteButtonImage1, greenButonImage1.width, greenButonImage1.height);
			ImageResize(&muteButtonImage2, greenButonImage1.width, greenButonImage1.height);
			ImageResize(&unMuteButtonImage1, greenButonImage1.width, greenButonImage1.height);
			ImageResize(&unMuteButtonImage2, greenButonImage1.width, greenButonImage1.height);
			playerUp = LoadTextureFromImage(playerUpImage);
			playerDown = LoadTextureFromImage(playerDownImage);
			playerLeft = LoadTextureFromImage(playerLeftImage);
			playerRight = LoadTextureFromImage(playerRightImage);
			ball = LoadTextureFromImage(ballImage);
			hole = LoadTextureFromImage(holeImage);
			grass = LoadTextureFromImage(grassImage);
			darkGrass = LoadTextureFromImage(darkGrassImage);
			soil = LoadTextureFromImage(soilImage);
			darkSoil = LoadTextureFromImage(darkSoilImage);
			spikes = LoadTextureFromImage(spikesImage);
			activeSpikes = LoadTextureFromImage(activeSpikesImage);
			star = LoadTextureFromImage(starImage);
			tinyStar = LoadTextureFromImage(tinyStarImage);
			greenButon1 = LoadTextureFromImage(greenButonImage1);
			greenButton2 = LoadTextureFromImage(greenButtonImage2);
			yellowButton1 = LoadTextureFromImage(yellowButtonImage1);
			yellowButton2 = LoadTextureFromImage(yellowButtonImage2);
			pauseButton1 = LoadTextureFromImage(pauseButtonImage1);
			pauseButton2 = LoadTextureFromImage(pauseButtonImage2);
			retryButton1 = LoadTextureFromImage(retryButtonImage1);
			retryButton2 = LoadTextureFromImage(retryButtonImage2);
			muteButton1 = LoadTextureFromImage(muteButtonImage1);
			muteButton2 = LoadTextureFromImage(muteButtonImage2);
			unMuteButton1 = LoadTextureFromImage(unMuteButtonImage1);
			unMuteButton2 = LoadTextureFromImage(unMuteButtonImage2);
			for (int i = 0; i < amountOfClouds; i++) {
				cloud[i] = LoadTextureFromImage(cloudImage[i]);
			}
		}
		static void cloudParallax(int currentCloud) {
			if (cloudPos[currentCloud].x + cloud[currentCloud].width <= 0) {
				cloudPos[currentCloud].x = static_cast<float>(screens::screenWidth);
			}
		}
		void drawBackClouds() {
			for (int i = 2; i < amountOfClouds; i++) {
				cloudPos[i].x -= cloudSpeed[i] * GetFrameTime();
				DrawTexture(cloud[i], static_cast<int>(cloudPos[i].x), static_cast<int>(cloudPos[i].y), WHITE);
				cloudParallax(i);
			}
		}
		void drawFrontClouds() {
			for (int i = 0; i < amountOfClouds/2; i++) {
				cloudPos[i].x -= cloudSpeed[i] * GetFrameTime();
				DrawTexture(cloud[i], static_cast<int>(cloudPos[i].x), static_cast<int>(cloudPos[i].y),
					Fade(WHITE, 0.4f));
				cloudParallax(i);
			}
		}
		void deinit() {
			UnloadImage(playerDownImage);
			UnloadImage(playerUpImage);
			UnloadImage(playerLeftImage);
			UnloadImage(playerRightImage);
			UnloadImage(ballImage);
			UnloadImage(holeImage);
			UnloadImage(grassImage);
			UnloadImage(darkGrassImage);
			UnloadImage(soilImage);
			UnloadImage(darkSoilImage);
			UnloadImage(spikesImage);
			UnloadImage(activeSpikesImage);
			UnloadImage(starImage);
			UnloadImage(tinyStarImage);
			UnloadImage(greenButonImage1);
			UnloadImage(greenButtonImage2);
			UnloadImage(yellowButtonImage1);
			UnloadImage(yellowButtonImage2);
			UnloadImage(pauseButtonImage1);
			UnloadImage(pauseButtonImage2);
			UnloadImage(retryButtonImage1);
			UnloadImage(retryButtonImage2);
			UnloadImage(muteButtonImage1);
			UnloadImage(muteButtonImage2);
			UnloadImage(unMuteButtonImage1);
			UnloadImage(unMuteButtonImage2);
			UnloadTexture(playerDown);
			UnloadTexture(playerUp);
			UnloadTexture(playerLeft);
			UnloadTexture(playerRight);
			UnloadTexture(ball);
			UnloadTexture(hole);
			UnloadTexture(grass);
			UnloadTexture(darkGrass);
			UnloadTexture(soil);
			UnloadTexture(darkSoil);
			UnloadTexture(spikes);
			UnloadTexture(activeSpikes);
			UnloadTexture(star);
			UnloadTexture(tinyStar);
			UnloadTexture(greenButon1);
			UnloadTexture(greenButton2);
			UnloadTexture(yellowButton1);
			UnloadTexture(yellowButton2);
			UnloadTexture(pauseButton1);
			UnloadTexture(pauseButton2);
			UnloadTexture(retryButton1);
			UnloadTexture(retryButton2);
			UnloadTexture(muteButton1);
			UnloadTexture(muteButton2);
			UnloadTexture(unMuteButton1);
			UnloadTexture(unMuteButton2);
			for (int i = 0; i < amountOfClouds; i++) {
				UnloadImage(cloudImage[i]);
				UnloadTexture(cloud[i]);
			}
		}
	}
}