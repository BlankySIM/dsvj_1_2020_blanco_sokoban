#ifndef GAME_H
#define GAME_H
#include "raylib.h"
#include "screens_manager/screens.h"
#include "scenes/credits/credits.h"
#include "scenes/gameplay/gameplay.h"
#include "scenes/menu/menu.h"
#include "scenes/results/results.h"

namespace sokoban {

	void run();
}
#endif
