#include "game.h"

namespace sokoban {

	static void init() {
		InitWindow(screens::screenWidth, screens::screenHeight, "Puzzle Maze");
		SetTargetFPS(screens::targetFPS);
		InitAudioDevice();
		menu::init();
		gameplay::init();
		credits::init();
		results::init();
	}
	static void inputs() {
		switch (screens::currentScreen) {
		case screens::gameplay_screen:
			gameplay::inputs();
			break;
		case screens::menu_screen:
			menu::inputs();
			break;
		case screens::credits_screen:
			credits::inputs();
			break;
		case screens::results_screen:
			results::inputs();
			break;
		}
	}
	static void update() {
		switch (screens::currentScreen) {
		case screens::gameplay_screen:
			gameplay::update();
			break;
		case screens::menu_screen:
			menu::update();
			break;
		case screens::credits_screen:
			credits::update();
			break;
		case screens::results_screen:
			results::update();
			break;
		}
	}
	static void draw() {
		BeginDrawing();
		ClearBackground(BLACK);
		switch (screens::currentScreen) {
		case screens::gameplay_screen:
			gameplay::draw();
			break;
		case screens::menu_screen:
			menu::draw();
			break;
		case screens::credits_screen:
			credits::draw();
			break;
		case screens::results_screen:
			results::draw();
			break;
		}
		EndDrawing();
	}
	static void deinit() {
		credits::deinit();
		gameplay::deinit();
		menu::deinit();
		results::deinit();
		CloseWindow();
	}
	void run() {
		sokoban::init();
		while (!WindowShouldClose()) {
			sokoban::inputs();
			sokoban::update();
			sokoban::draw();
		}
		sokoban::deinit();
	}
}