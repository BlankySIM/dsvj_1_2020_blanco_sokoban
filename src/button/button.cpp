#include "button.h"

namespace sokoban {
	namespace button {

		float normalButtonW = 0;
		float normalButtonH = 0;
		float tinyButtonW = 0;
		float tinyButtonH = 0;

		void initButtonSizes() {
			normalButtonW = static_cast<float>(sprites::yellowButton1.width);
			normalButtonH = static_cast<float>(sprites::yellowButton1.height);
			tinyButtonH = static_cast<float>(sprites::greenButon1.height);
			tinyButtonW = static_cast<float>(sprites::greenButon1.width);
		}
		void update(BUTTON &buttonToCheck, Vector2 mousePosition) {
			if (CheckCollisionPointRec(mousePosition, buttonToCheck.rec)
				&& IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
				buttonToCheck.active = true;
				PlaySound(audio::buttonSfx);
			}
		}
		void draw(BUTTON buttonToDraw, Vector2 mousePosition) {
			if (CheckCollisionPointRec(mousePosition, buttonToDraw.rec)) {
				#if DEBUG
				DrawRectangle(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), Fade(WHITE, 0.4f));
				DrawRectangleLines(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), GREEN);
				#endif
				DrawTexture(buttonToDraw.selectedSprite, static_cast<int>(buttonToDraw.rec.x),
					static_cast<int>(buttonToDraw.rec.y), WHITE);
			}
			else {
				#if DEBUG
				DrawRectangle(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), Fade(GRAY, 0.4f));
				DrawRectangleLines(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), DARKGREEN);
				#endif
				DrawTexture(buttonToDraw.sprite, static_cast<int>(buttonToDraw.rec.x),
					static_cast<int>(buttonToDraw.rec.y), WHITE);
				DrawTexture(buttonToDraw.sprite, static_cast<int>(buttonToDraw.rec.x),
					static_cast<int>(buttonToDraw.rec.y), Fade(GRAY, 0.3f));
			}
		}

	}
}