#ifndef BUTTON_H
#define	BUTTON_H
#include "raylib.h"
#include "screens_manager/screens.h"
#include "sprites/sprites.h"
#include "audio/audio.h"

namespace sokoban {
	namespace button {

		extern float normalButtonW;
		extern float normalButtonH;
		extern float tinyButtonW;
		extern float tinyButtonH;
		struct BUTTON {
			Rectangle rec;
			bool active;
			Texture2D sprite;
			Texture2D selectedSprite;
		};

		void initButtonSizes();
		void update(BUTTON &buttonToCheck, Vector2 mousePosition);
		void draw(BUTTON buttonToDraw, Vector2 mousePosition);
	}
}
#endif
