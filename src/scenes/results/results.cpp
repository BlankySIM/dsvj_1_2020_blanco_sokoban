#include "results.h"

namespace sokoban {
	namespace results {

		int initialStarsPosition;
		int starsW;
		int starsH;
		int starsOffset;
		button::BUTTON restartButton;
		button::BUTTON menuButton;
		Vector2 mousePosition;

		static void drawStars() {
			for (int i = 0; i < level_loader::threeStars; i++) {
				if (i < player::sokobanPlayer.obtainedStars) {
					#if DEBUG
					DrawRectangleLines(initialStarsPosition + ((starsOffset + starsW) * i),
						screens::screenZero + screens::screenHeight / 5, starsW, starsH, YELLOW);
					#endif
					DrawTexture(sprites::star, initialStarsPosition + ((starsOffset + starsW) * i),
						screens::screenZero + screens::screenHeight / 5, WHITE);
				}
				else {
					#if DEBUG
					DrawRectangleLines(initialStarsPosition + ((starsOffset + starsW) * i),
						screens::screenZero + screens::screenHeight / 5, starsW, starsH, Fade(YELLOW, 0.2f));
					#endif
					DrawTexture(sprites::star, initialStarsPosition + ((starsOffset + starsW) * i),
						screens::screenZero + screens::screenHeight / 5, Fade(WHITE, 0.15f));
				}
			}		
		}
		void init() {
			starsOffset = screens::screenWidth / 50;
			starsW = sprites::star.width;
			starsH = sprites::star.height;
			initialStarsPosition = screens::middleScreenW - static_cast<int>((starsW * 1.50f) + starsOffset);
			restartButton.sprite = sprites::yellowButton1;
			restartButton.selectedSprite = sprites::yellowButton2;
			menuButton.sprite = sprites::yellowButton1;
			menuButton.selectedSprite = sprites::yellowButton2;
			restartButton.rec.width = button::normalButtonW;
			restartButton.rec.height = button::normalButtonH;
			menuButton.rec = restartButton.rec;
			restartButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			restartButton.rec.y = screens::middleScreenH - (screens::screenHeight / 10.0f);
			menuButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			menuButton.rec.y = restartButton.rec.y + restartButton.rec.height + (screens::middleScreenH / 5);
		}
		void inputs() {
			mousePosition = GetMousePosition();
		}
		void update() {
			PlayMusicStream(audio::gameplayMusic);
			UpdateMusicStream(audio::gameplayMusic);
			button::update(restartButton, mousePosition);
			button::update(menuButton, mousePosition);
			if (restartButton.active) {
				restartButton.active = !restartButton.active;
				level_loader::initLevel(level_loader::selectedLevel);
				if (!audio::muted) {
					audio::setGameplayVolume();
				}
				screens::currentScreen = screens::gameplay_screen;
			}
			else if (menuButton.active) {
				menuButton.active = !menuButton.active;
				screens::currentScreen = screens::menu_screen;
				if (!audio::muted) {
					audio::setGameplayVolume();
				}
				StopMusicStream(audio::gameplayMusic);
			}
		}
		void draw() {
			DrawRectangle(screens::screenZero, screens::screenZero,
				screens::screenWidth, screens::screenHeight, SKYBLUE);
			sprites::drawBackClouds();
			sprites::drawFrontClouds();
			DrawRectangle(screens::screenZero, screens::screenZero,
				screens::screenWidth, screens::screenHeight, Fade(BLACK, 0.6f));
			DrawText("You won", static_cast<int>(screens::middleScreenW - (screens::screenWidth / 14.0f)),
				screens::screenZero + (screens::middleScreenH / 20), screens::maxFontSize, WHITE);
			drawStars();
			button::draw(restartButton, mousePosition);
			button::draw(menuButton, mousePosition);
			DrawText("Restart", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 22,
				static_cast<int>(restartButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
			DrawText("Menu", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 34,
				static_cast<int>(menuButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
		}
		void deinit() {

		}
	}
}