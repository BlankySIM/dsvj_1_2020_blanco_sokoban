#ifndef RESULTS_H
#define RESULTS_H
#include "raylib.h"
#include "level_loader/level_loader.h"
#include "screens_manager/screens.h"
#include "button/button.h"
#include "sprites/sprites.h"
#include "audio/audio.h"

namespace sokoban {
	namespace results {

		void init();
		void inputs();
		void update();
		void draw();
		void deinit();
	}
}
#endif
