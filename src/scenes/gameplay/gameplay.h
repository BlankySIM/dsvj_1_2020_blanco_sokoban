#ifndef GAMEPLAY_H
#define GAMEPLAY_H
#include "ground/ground.h"
#include "player/player.h"
#include "ball/ball.h"
#include "hole/hole.h"
#include "level_loader/level_loader.h"
#include "button/button.h"
#include "audio/audio.h"

namespace sokoban {
	namespace gameplay {

		void init();
		void inputs();
		void update();
		void draw();
		void deinit();
	}
}
#endif
