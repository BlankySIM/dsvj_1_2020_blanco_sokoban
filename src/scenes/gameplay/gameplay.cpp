#include "gameplay.h"

namespace sokoban {
	namespace gameplay {

		button::BUTTON unPauseButton;
		button::BUTTON menuButton;
		button::BUTTON pauseButton;
		button::BUTTON restartButton;
		button::BUTTON muteButton;
		button::BUTTON unMuteButton;
		Vector2 mousePosition;

		void init() {
			ground::init();
			player::init();
			ball::init();
			hole::init();
			spikes::init();
			level_loader::init();
			pauseButton.sprite = sprites::pauseButton1;
			pauseButton.selectedSprite = sprites::pauseButton2;
			unPauseButton.sprite = sprites::yellowButton1;
			unPauseButton.selectedSprite = sprites::yellowButton2;
			menuButton.sprite = sprites::yellowButton1;
			menuButton.selectedSprite = sprites::yellowButton2;
			restartButton.sprite = sprites::retryButton1;
			restartButton.selectedSprite = sprites::retryButton2;
			muteButton.sprite = sprites::muteButton1;
			muteButton.selectedSprite = sprites::muteButton2;
			unMuteButton.sprite = sprites::unMuteButton1;
			unMuteButton.selectedSprite = sprites::unMuteButton2;
			pauseButton.rec.height = button::tinyButtonH;
			pauseButton.rec.width = button::tinyButtonW;
			restartButton.rec = pauseButton.rec;
			pauseButton.rec.x = screens::middleScreenW - ((pauseButton.rec.width + (screens::screenWidth/100)));
			pauseButton.rec.y = screens::screenZero + static_cast<float>(screens::screenHeight / 200);
			restartButton.rec.x= screens::middleScreenW  + static_cast<float>(screens::screenWidth / 100);
			restartButton.rec.y = pauseButton.rec.y;
			unPauseButton.rec.width = button::normalButtonW;
			unPauseButton.rec.height = button::normalButtonH;
			menuButton.rec = unPauseButton.rec;
			unPauseButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			unPauseButton.rec.y = screens::middleScreenH - (screens::screenHeight / 10.0f);
			menuButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			menuButton.rec.y = unPauseButton.rec.y + unPauseButton.rec.height + (screens::middleScreenH / 5);
			muteButton.rec = pauseButton.rec;
			muteButton.rec.x = screens::screenZero + screens::screenWidth / 50.0f;
			muteButton.rec.y = screens::screenZero + screens::screenHeight / 50.0f;
			unMuteButton.rec = muteButton.rec;
		}
		void inputs() {
			player::inputs();
			mousePosition = GetMousePosition();
		}
		void update() {
			PlayMusicStream(audio::gameplayMusic);
			UpdateMusicStream(audio::gameplayMusic);
			if (!pauseButton.active) {
				button::update(pauseButton, mousePosition);
				button::update(restartButton, mousePosition);
				player::update();
				ball::update();
				spikes::update();
				if (ball::ball.inHole) {
					level_loader::setLevelStars();
					if (!audio::muted) {
						audio::setPausedGameplayVolume();
					}
					screens::currentScreen = screens::results_screen;
					level_loader::unlockLevels();
					PlaySound(audio::victorySfx);
				}
				if (!player::sokobanPlayer.alive) {
					player::sokobanPlayer.alive = true;
					level_loader::initLevel(level_loader::selectedLevel);
				}
				if (restartButton.active) {
					restartButton.active = !restartButton.active;
					level_loader::initLevel(level_loader::selectedLevel);
				}
			}
			else {
				if (!audio::muted) {
					audio::setPausedGameplayVolume();
				}
				button::update(unPauseButton, mousePosition);
				button::update(menuButton, mousePosition);
				if (unPauseButton.active) {
					if (!audio::muted) {
						audio::setGameplayVolume();
					}
					unPauseButton.active = !unPauseButton.active;
					pauseButton.active = !pauseButton.active;
				}
				else if (menuButton.active) {
					menuButton.active = !menuButton.active;
					pauseButton.active = !pauseButton.active;
					screens::currentScreen = screens::menu_screen;
					if (!audio::muted) {
						audio::setGameplayVolume();
					}
					StopMusicStream(audio::gameplayMusic);
				}
				switch (audio::muted) {
				case true:
					button::update(unMuteButton, mousePosition);
					if (unMuteButton.active) {
						unMuteButton.active = !unMuteButton.active;
						audio::muted = !audio::muted;
						audio::setVolumes();
					}
					break;
				case false:
					button::update(muteButton, mousePosition);
					if (muteButton.active) {
						muteButton.active = !muteButton.active;
						audio::muted = !audio::muted;
						audio::setMutedVolumes();
					}
					break;
				}
			}
		}
		static void drawLevel1Hints() {
			DrawRectangle(screens::screenZero, screens::screenZero + screens::screenHeight / 10,
				screens::screenWidth, static_cast<int>(screens::screenHeight / 2.2f), Fade(BLACK, 0.75f));
			DrawText("Move the player with", screens::screenZero + screens::screenWidth / 30,
				static_cast<int>(screens::screenZero + screens::screenHeight / 7.5f), screens::medFontSize, WHITE);
			DrawTexture(sprites::greenButon1, screens::screenZero + static_cast<int>(screens::screenWidth / 6.8f),
				screens::screenZero + screens::screenHeight / 4, WHITE);
			DrawTexture(sprites::greenButon1, screens::screenZero + static_cast<int>(screens::screenWidth / 6.8f),
				screens::screenZero + screens::screenHeight / 4 + sprites::greenButon1.height, WHITE);
			DrawTexture(sprites::greenButon1, screens::screenZero
				+ static_cast<int>(screens::screenWidth / 6.8f) + sprites::greenButon1.width,
				screens::screenZero + screens::screenHeight / 4 + sprites::greenButon1.height, WHITE);
			DrawTexture(sprites::greenButon1, screens::screenZero
				+ static_cast<int>(screens::screenWidth / 6.8f) - sprites::greenButon1.width,
				screens::screenZero + screens::screenHeight / 4 + sprites::greenButon1.height, WHITE);
			DrawText("W", screens::screenZero
				+ static_cast<int>(screens::screenWidth / 6.8f) + static_cast<int>(screens::screenWidth / 125),
				screens::screenZero + screens::screenHeight / 4, screens::medFontSize, BLACK);
			DrawText("S", screens::screenZero + static_cast<int>(screens::screenWidth / 6.8f) 
				+ static_cast<int>(screens::screenWidth / 125),
				screens::screenZero + screens::screenHeight / 4 + sprites::greenButon1.height, screens::medFontSize, BLACK);
			DrawText("D", screens::screenZero
				+ static_cast<int>(screens::screenWidth / 6.8f) + sprites::greenButon1.width
				+ static_cast<int>(screens::screenWidth / 125),
				screens::screenZero + screens::screenHeight / 4 + sprites::greenButon1.height, screens::medFontSize, BLACK);
			DrawText("A", screens::screenZero
				+ static_cast<int>(screens::screenWidth / 6.8f) - sprites::greenButon1.width
				+ static_cast<int>(screens::screenWidth / 125),
				screens::screenZero + screens::screenHeight / 4 + sprites::greenButon1.height, screens::medFontSize, BLACK);
			DrawText("Take the ball", static_cast<int>(screens::screenWidth/2.8f),
				static_cast<int>(screens::screenZero + screens::screenHeight / 7.6f), screens::medFontSize, YELLOW);
			DrawText("to the hole", static_cast<int>(screens::screenWidth / 2.8f),
				static_cast<int>(screens::screenZero + screens::screenHeight / 7.6f + (screens::maxFontSize * 2)),
				screens::medFontSize, YELLOW);
			DrawTexture(sprites::ball, static_cast<int>(screens::screenWidth / 1.85f),
				static_cast<int>(screens::screenZero + screens::screenHeight / 8.8f), WHITE);
			DrawTexture(sprites::grass, static_cast<int>(screens::screenWidth / 1.85f),
				static_cast<int>(screens::screenZero + screens::screenHeight / 8.8f) + (screens::maxFontSize * 2), WHITE);
			DrawTexture(sprites::hole, static_cast<int>(screens::screenWidth / 1.85f),
				static_cast<int>(screens::screenZero + screens::screenHeight / 8.8f) + (screens::maxFontSize * 2), WHITE);
			DrawText("Restart the level with", static_cast<int>(screens::screenWidth / 1.5f),
				static_cast<int>(screens::screenZero + screens::screenHeight / 7.6f), screens::medFontSize, PINK);
			DrawText("the retry button", static_cast<int>(screens::screenWidth / 1.5f),
				static_cast<int>(screens::screenZero + screens::screenHeight / 7.6f + (screens::maxFontSize * 2)),
				screens::medFontSize, PINK);
			DrawTexture(sprites::retryButton1, static_cast<int>(screens::screenWidth / 1.1f),
				static_cast<int>(screens::screenZero + screens::screenHeight / 8.8f) + (screens::maxFontSize * 2), WHITE);
		}
		void draw() {
			DrawRectangle(screens::screenZero, screens::screenZero,
				screens::screenWidth, screens::screenHeight, SKYBLUE);
			sprites::drawBackClouds();
			ground::draw();
			spikes::draw();
			player::draw();
			ball::draw();
			hole::draw();
			sprites::drawFrontClouds();
			DrawText(FormatText("Steps: %0i", player::sokobanPlayer.steps), screens::screenZero + screens::screenWidth / 100,
				screens::screenZero + screens::screenHeight / 100, screens::medFontSize, WHITE);
			if (level_loader::selectedLevel == 0) {
				drawLevel1Hints();
			}
			if (!pauseButton.active) {
				button::draw(pauseButton, mousePosition);
				button::draw(restartButton, mousePosition);
			}
			else {
				DrawRectangle(screens::screenZero, screens::screenZero,
					screens::screenWidth, screens::screenHeight, Fade(BLACK, 0.9f));
				button::draw(unPauseButton, mousePosition);
				button::draw(menuButton, mousePosition);
				switch (audio::muted) {
				case true:
					button::draw(unMuteButton, mousePosition);
					break;
				case false:
					button::draw(muteButton, mousePosition);
					break;
				}
				DrawText("Continue", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 20,
					static_cast<int>(unPauseButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
				DrawText("Menu", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 34,
					static_cast<int>(menuButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
			}
		}
		void deinit() {

		}
	}
}