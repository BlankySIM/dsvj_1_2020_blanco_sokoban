#ifndef MENU_H
#define MENU_H
#include "raylib.h"
#include "button/button.h"
#include "level_loader/level_loader.h"
#include "sprites/sprites.h"
#include "audio/audio.h"

namespace sokoban {
	namespace menu {

		void init();
		void inputs();
		void update();
		void draw();
		void deinit();
	}
}
#endif
