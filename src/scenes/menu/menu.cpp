#include "menu.h"

namespace sokoban {
	namespace menu {

		bool levelsSelection = false;
		int levelButtonsOffset;
		int starsW;
		int starsH;
		int starsOffset;
		Rectangle levelsContainer;
		button::BUTTON startButton;
		button::BUTTON creditsButton;
		button::BUTTON backButton;
		button::BUTTON levelButton[level_loader::amountOfLevels];
		button::BUTTON muteButton;
		button::BUTTON unMuteButton;
		Vector2 mousePosition;

		static void drawStars() {
			for (int j = 0; j < level_loader::amountOfLevels; j++) {
				for (int i = 0; i < level_loader::threeStars; i++) {
					if (i < level_loader::levels[j].stars) {
						#if DEBUG
						DrawRectangleLines(static_cast<int>((levelButton[j].rec.x + (levelButton[j].rec.width/2)) - ((starsW * 1.50f) +
							starsOffset)) + ((starsOffset + starsW) * i),
							static_cast<int>(levelButton[j].rec.y + screens::screenHeight / 100 + levelButton[j].rec.height),
							starsW, starsH, Fade(YELLOW, 0.6f));
						#endif
						DrawTexture(sprites::tinyStar, static_cast<int>((levelButton[j].rec.x +
							(levelButton[j].rec.width / 2)) - ((starsW * 1.50f) +
								starsOffset)) + ((starsOffset + starsW) * i),
							static_cast<int>(levelButton[j].rec.y + screens::screenHeight / 100 +
								levelButton[j].rec.height), WHITE);
					}
					else {
						#if DEBUG
						DrawRectangleLines(static_cast<int>((levelButton[j].rec.x + (levelButton[j].rec.width / 2)) - ((starsW * 1.50f) + 
							starsOffset)) + ((starsOffset + starsW) * i),
							static_cast<int>(levelButton[j].rec.y + screens::screenHeight / 100 + levelButton[j].rec.height),
							starsW, starsH, Fade(YELLOW, 0.03f));
						#endif
						DrawTexture(sprites::tinyStar, static_cast<int>((levelButton[j].rec.x +
							(levelButton[j].rec.width / 2)) - ((starsW * 1.50f) +
								starsOffset)) + ((starsOffset + starsW) * i),
							static_cast<int>(levelButton[j].rec.y + screens::screenHeight / 100 +
								levelButton[j].rec.height), Fade(WHITE, 0.05f));
					}
				}
			}
		}
		void init() {
			audio::init();
			sprites::init();
			button::initButtonSizes();
			starsOffset = screens::screenWidth / 150;
			starsW = sprites::tinyStar.width;
			starsH = sprites::tinyStar.height;
			for (int i = 0; i < level_loader::amountOfLevels; i++) {
				levelButton[i].sprite = sprites::greenButon1;
				levelButton[i].selectedSprite = sprites::greenButton2;
				levelButton[i].rec.width = button::tinyButtonW;
				levelButton[i].rec.height = button::tinyButtonH;
				if (i >= level_loader::amountOfLevels / 2) {
					levelButton[i].rec.y = static_cast<float>(screens::middleScreenH + screens::screenHeight / 10);
				}
				else {
					levelButton[i].rec.y = static_cast<float>(screens::middleScreenH - screens::screenHeight / 10);
				}
			}
			startButton.sprite = sprites::yellowButton1;
			startButton.selectedSprite = sprites::yellowButton2;
			backButton.sprite = sprites::yellowButton1;
			backButton.selectedSprite = sprites::yellowButton2;
			creditsButton.sprite = sprites::yellowButton1;
			creditsButton.selectedSprite = sprites::yellowButton2;
			muteButton.sprite = sprites::muteButton1;
			muteButton.selectedSprite = sprites::muteButton2;
			unMuteButton.sprite = sprites::unMuteButton1;
			unMuteButton.selectedSprite = sprites::unMuteButton2;
			levelButton[1].rec.x = screens::middleScreenW - (button::tinyButtonW / 2);
			levelButton[0].rec.x = levelButton[1].rec.x - ((button::normalButtonW / 3) * 2);
			levelButton[2].rec.x = levelButton[1].rec.x + ((button::normalButtonW / 3) * 2);
			levelButton[3].rec.x = levelButton[0].rec.x;
			levelButton[4].rec.x = levelButton[1].rec.x;
			levelButton[5].rec.x = levelButton[2].rec.x;
			startButton.rec.width = button::normalButtonW;
			startButton.rec.height = button::normalButtonH;
			creditsButton.rec = startButton.rec;
			backButton.rec = startButton.rec;
			startButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			startButton.rec.y = screens::middleScreenH - (screens::screenHeight / 10.0f);
			creditsButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			creditsButton.rec.y = startButton.rec.y + startButton.rec.height + (screens::middleScreenH / 5);
			backButton.rec.x = screens::screenZero + screens::screenWidth / 80.0f;
			backButton.rec.y = (screens::screenHeight - backButton.rec.height) - screens::middleScreenH / 45.0f;
			levelsContainer.x = levelButton[0].rec.x - button::tinyButtonW;
			levelsContainer.y = levelButton[0].rec.y - button::tinyButtonH;
			levelsContainer.width = levelButton[2].rec.x + (button::tinyButtonW * 2) - levelsContainer.x;
			levelsContainer.height = levelButton[3].rec.y + (((button::tinyButtonH * 2) - levelsContainer.y) + starsH);
			muteButton.rec = levelButton[0].rec;
			muteButton.rec.x = screens::screenZero + screens::screenWidth / 50.0f;
			muteButton.rec.y = screens::screenZero + screens::screenHeight / 50.0f;
			unMuteButton.rec = muteButton.rec;
		}
		void inputs() {
			mousePosition = GetMousePosition();
		}
		void update() {
			PlayMusicStream(audio::menuMusic);
			UpdateMusicStream(audio::menuMusic);
			if (levelsSelection) {
				for (int i = 0; i < level_loader::amountOfLevels; i++) {
					if (level_loader::levels[i].unlocked) {
						button::update(levelButton[i], mousePosition);
					}
				}
			}
			else {
				button::update(startButton, mousePosition);
				button::update(creditsButton, mousePosition);
			}
			if (startButton.active) {
				startButton.active = !startButton.active;
				levelsSelection = true;
			}
			else if (creditsButton.active) {
				creditsButton.active = !creditsButton.active;
				screens::currentScreen = screens::credits_screen;
			}
			else {
				button::update(backButton, mousePosition);
				if (backButton.active) {
					backButton.active = !backButton.active;
					levelsSelection = !levelsSelection;
				}
				for (int i = 0; i < level_loader::amountOfLevels; i++) {
					if (level_loader::levels[i].unlocked && levelButton[i].active) {
						levelsSelection = false;
						levelButton[i].active = !levelButton[i].active;
						level_loader::selectedLevel = i;
						level_loader::initLevel(level_loader::selectedLevel);
						screens::currentScreen = screens::gameplay_screen;
						StopMusicStream(audio::menuMusic);
					}
				}
			}
			switch (audio::muted) {
			case true:
				button::update(unMuteButton, mousePosition);
				if (unMuteButton.active) {
					unMuteButton.active = !unMuteButton.active;
					audio::muted = !audio::muted;
					audio::setVolumes();
				}
				break;
			case false:
				button::update(muteButton, mousePosition);
				if (muteButton.active) {
					muteButton.active = !muteButton.active;
					audio::muted = !audio::muted;
					audio::setMutedVolumes();
				}
				break;
			}
		}
		void draw() {
			DrawRectangle(screens::screenZero, screens::screenZero,
				screens::screenWidth, screens::screenHeight, SKYBLUE);
			sprites::drawBackClouds();
			sprites::drawFrontClouds();
			DrawRectangle(screens::screenZero, screens::screenZero,
				screens::screenWidth, screens::screenHeight, Fade(BLACK, 0.1f));
			switch (audio::muted) {
			case true:
				button::draw(unMuteButton, mousePosition);
				break;
			case false:
				button::draw(muteButton, mousePosition);
				break;
			}
			if (levelsSelection) {
				DrawText("Level select", static_cast<int>(screens::middleScreenW - (screens::screenWidth / 9.5f)),
					screens::screenZero + (screens::middleScreenH / 20), screens::maxFontSize, WHITE);
				DrawRectangle(static_cast<int>(levelsContainer.x), static_cast<int>(levelsContainer.y),
					static_cast<int>(levelsContainer.width), static_cast<int>(levelsContainer.height), Fade(DARKGRAY, 0.7f));
				for (int i = 0; i < level_loader::amountOfLevels; i++) {
					button::draw(levelButton[i], mousePosition);
					if (!level_loader::levels[i].unlocked) {
						DrawTexture(sprites::greenButon1, static_cast<int>(levelButton[i].rec.x),
							static_cast<int>(levelButton[i].rec.y), DARKGRAY);
					}
					DrawText(FormatText("%i", i+1), static_cast<int>((levelButton[i].rec.x) + screens::medFontSize/2),
						static_cast<int>(((levelButton[i].rec.y) + levelButton[i].rec.height / 2) -
						screens::medFontSize / 2), screens::medFontSize, BLACK);
					drawStars();
				}
				button::draw(backButton, mousePosition);
				DrawText("Back", static_cast<int>(backButton.rec.x) + screens::screenWidth / 21,
					static_cast<int>(backButton.rec.y) + screens::screenHeight / 32, screens::medFontSize, BLACK);
			}
			else {
				DrawText("Puzzle Maze", static_cast<int>(screens::middleScreenW - (screens::screenWidth / 9.6f)),
					screens::screenZero + (screens::middleScreenH / 20), screens::maxFontSize, WHITE);
				button::draw(startButton, mousePosition);
				button::draw(creditsButton, mousePosition);
				DrawText("Start", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 27,
					static_cast<int>(startButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
				DrawText("Credits", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 20,
					static_cast<int>(creditsButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
				DrawText("v1.0.0", static_cast<int>(screens::screenZero + screens::screenWidth / 100.0f),
					static_cast<int>(screens::screenHeight - (screens::medFontSize + screens::screenHeight / 100)),
					screens::medFontSize, BLACK);
			}
		}
		void deinit() {
			audio::deinit();
			sprites::deinit();
		}
	}
}