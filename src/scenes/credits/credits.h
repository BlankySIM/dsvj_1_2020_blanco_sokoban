#ifndef CREDITS_H
#define CREDITS_H
#include "raylib.h"
#include "screens_manager/screens.h"
#include "button/button.h"
#include "sprites/sprites.h"
#include "audio/audio.h"

namespace sokoban {
	namespace credits {

		void init();
		void inputs();
		void update();
		void draw();
		void deinit();
	}
}
#endif
