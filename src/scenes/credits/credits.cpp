#include "credits.h"

namespace sokoban {
	namespace credits {

		button::BUTTON backButton;
		Vector2 mousePosition;

		void init() {
			backButton.sprite = sprites::yellowButton1;
			backButton.selectedSprite = sprites::yellowButton2;
			backButton.rec.width = button::normalButtonW;
			backButton.rec.height = button::normalButtonH;
			backButton.rec.x = screens::screenZero + screens::screenWidth / 80.0f;
			backButton.rec.y = (screens::screenHeight - backButton.rec.height) - screens::middleScreenH / 45.0f;
		}
		void inputs() {
			mousePosition = GetMousePosition();
		}
		void update() {
			PlayMusicStream(audio::menuMusic);
			UpdateMusicStream(audio::menuMusic);
			button::update(backButton, mousePosition);
			if (backButton.active) {
				backButton.active = !backButton.active;
				screens::currentScreen = screens::menu_screen;
			}
		}
		void draw() {
			DrawRectangle(screens::screenZero, screens::screenZero,
				screens::screenWidth, screens::screenHeight, SKYBLUE);
			sprites::drawBackClouds();
			sprites::drawFrontClouds();
			DrawRectangle(screens::screenZero, screens::screenZero,
				screens::screenWidth, screens::screenHeight, Fade(BLACK, 0.7f));
			DrawText("Credits", screens::middleScreenW - (screens::screenWidth / 17),
				screens::screenZero + (screens::middleScreenH / 20), screens::maxFontSize, WHITE);
			button::draw(backButton, mousePosition);
			DrawText("Back", static_cast<int>(backButton.rec.x) + screens::screenWidth / 21,
				static_cast<int>(backButton.rec.y) + screens::screenHeight / 32, screens::medFontSize, BLACK);
			DrawText("GameDev:", screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + screens::screenHeight / 10, screens::medFontSize, SKYBLUE);
			DrawText("- Blanco Juan Simon: blancojuansimon@gmail.com || https://blankysim.itch.io", screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + screens::screenHeight / 10 + screens::maxFontSize, screens::minFontsize, WHITE);
			DrawText("Game Art:", screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + screens::screenHeight / 10 + screens::screenHeight / 10, screens::medFontSize, SKYBLUE);
			DrawText("- Characters and level art by: https://comigo.itch.io/", screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + screens::screenHeight / 10 + screens::screenHeight / 10 + screens::maxFontSize, screens::minFontsize, WHITE);
			DrawText("- UI by: https://madfireongames.com/", screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + screens::screenHeight / 10 + screens::screenHeight / 5, screens::minFontsize, WHITE);
			DrawText("Game Audio", screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + screens::screenHeight / 11 + screens::screenHeight / 5 + screens::medFontSize, screens::medFontSize, SKYBLUE);			
			DrawText("- Gameplay music by: https://opengameart.org/users/fupi", screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + screens::screenHeight / 10 +
				static_cast<int>(screens::screenHeight / 3.5f), screens::minFontsize, WHITE);
			DrawText("- Menu music by: https://opengameart.org/users/akikazer", screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + screens::screenHeight / 10 +
				static_cast<int>(screens::screenHeight / 3.0f), screens::minFontsize, WHITE);
			DrawText("- SFX by:", screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + screens::screenHeight / 10 +
				static_cast<int>(screens::screenHeight / 2.6f), screens::minFontsize, SKYBLUE);
			DrawText("- https://freesfx.co.uk/", screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + screens::screenHeight / 10 +
				static_cast<int>(screens::screenHeight / 2.3f), screens::minFontsize, WHITE);
			DrawText("- https://freesound.org/people/suntemple/sounds/241809/", screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + screens::screenHeight / 10 +
				static_cast<int>(screens::screenHeight / 2.08f), screens::minFontsize, WHITE);
		}
		void deinit() {

		}
	}
}