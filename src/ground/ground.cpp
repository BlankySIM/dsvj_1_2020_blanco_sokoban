#include "ground.h"

namespace sokoban {
	namespace ground {
		int groundTileW;
		int groundTileH;

		void init() {
			bool darkGroundTile=false;
			groundTileW = static_cast<int>(sprites::grass.width);
			groundTileH = static_cast<int>(sprites::grass.height);
			int initialPosx = static_cast<int>(screens::middleScreenW - ((maxGroundW / 2.0f) * groundTileW));
			int initialPosy = static_cast<int>(screens::screenZero + (screens::screenHeight / 6.0f));
			for (int i = 0; i < maxGroundW; i++) {
				for (int j = 0; j < maxGroundH; j++) {
					map[i][j].rec.x = static_cast<float>(initialPosx + (i * groundTileW));
					map[i][j].rec.y = static_cast<float>(initialPosy + (j * groundTileH));
					map[i][j].rec.height = static_cast<float>(groundTileH);
					map[i][j].rec.width = static_cast<float>(groundTileW);
					map[i][j].active = true;
					if (darkGroundTile) {
						map[i][j].color = DARKGREEN;
						map[i][j].sprite = sprites::darkGrass;
						map[i][j].soilSprite = sprites::darkSoil;
						darkGroundTile = !darkGroundTile;
					}
					else {
						map[i][j].color = GREEN;
						map[i][j].sprite = sprites::grass;
						map[i][j].soilSprite = sprites::soil;
						darkGroundTile = !darkGroundTile;
					}
				}
			}
		}
		void draw() {
			for (int i = 0; i < maxGroundW; i++) {
				for (int j = 0; j < maxGroundH; j++) {
					if (map[i][j].active) {
						#if DEBUG
						DrawRectangle(static_cast<int>(map[i][j].rec.x), static_cast<int>(map[i][j].rec.y),
							static_cast<int>(map[i][j].rec.width), static_cast<int>(map[i][j].rec.height), map[i][j].color);
						#endif
						DrawTexture(map[i][j].sprite, static_cast<int>(map[i][j].rec.x),
							static_cast<int>(map[i][j].rec.y), WHITE);
						if (!map[i][j+1].active) {
							DrawTexture(map[i][j].soilSprite, static_cast<int>(map[i][j].rec.x),
								static_cast<int>(map[i][j].rec.y + map[i][j].rec.height), WHITE);
						}
					}
				}
			}
		}
		extern GROUND map[maxGroundW][maxGroundH] = {0};
	}
}