#ifndef GROUND_H
#define GROUND_H
#include "raylib.h"
#include "screens_manager/screens.h"
#include "sprites/sprites.h"

namespace sokoban {
	namespace ground {

		const int maxGroundW = 13;
		const int maxGroundH = 13;
		struct GROUND {
			Rectangle rec;
			bool active;
			Color color;
			Texture2D sprite;
			Texture2D soilSprite;
		};

		void init();
		void draw();
		extern GROUND map[maxGroundW][maxGroundH];
	}
}
#endif
