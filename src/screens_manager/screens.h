#ifndef SCREENS_H
#define SCREENS_H

namespace sokoban {
	namespace screens {

		const int targetFPS = 60;
		extern int screenHeight;
		extern int screenWidth;
		extern int middleScreenH;
		extern int middleScreenW;
		extern int screenZero;
		extern int minFontsize;
		extern int medFontSize;
		extern int maxFontSize;
		enum scenes {
			gameplay_screen,
			menu_screen,
			credits_screen,
			results_screen,
		};

		extern scenes currentScreen;
	}
}
#endif
