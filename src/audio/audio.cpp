#include "audio.h"

namespace sokoban {
	namespace audio {

		float gameplayMusicVolume = 0.35f;
		float menuMusicVolume = 0.5f;
		float pausedGameplayVolume = 0.1f;
		float buttonSfxVolume = 0.5f;
		float victorySfxVolume = 0.5f;
		float spikeSwitchSfxVolume = 0.2f;
		float spikeHitSfxVolume = 0.7f;
		float mutedVolume = 0.0f;
		bool muted = false;
		Music gameplayMusic;
		Music menuMusic;
		Sound buttonSfx;
		Sound victorySfx;
		Sound spikeSwitchSfx;
		Sound spikeHitSfx;

		void init() {
			gameplayMusic = LoadMusicStream("res/music/gameplay_music.mp3");
			menuMusic = LoadMusicStream("res/music/menu_music.mp3");
			buttonSfx=LoadSound("res/sfx/button_sfx.mp3");
			victorySfx = LoadSound("res/sfx/level_completed_sfx.mp3");
			spikeSwitchSfx = LoadSound("res/sfx/spikes_switch_sfx.mp3");
			spikeHitSfx = LoadSound("res/sfx/spikes_impact_sfx.mp3");
			setVolumes();
		}
		void setVolumes() {
				SetMusicVolume(gameplayMusic, gameplayMusicVolume);
				SetMusicVolume(menuMusic, menuMusicVolume);
				SetSoundVolume(buttonSfx, buttonSfxVolume);
				SetSoundVolume(victorySfx, victorySfxVolume);
				SetSoundVolume(spikeHitSfx, spikeHitSfxVolume);
				SetSoundVolume(spikeSwitchSfx, spikeSwitchSfxVolume);
		}
		void setMutedVolumes() {
				SetMusicVolume(gameplayMusic, mutedVolume);
				SetMusicVolume(menuMusic, mutedVolume);
				SetSoundVolume(buttonSfx, mutedVolume);
				SetSoundVolume(victorySfx, mutedVolume);
				SetSoundVolume(spikeHitSfx, mutedVolume);
				SetSoundVolume(spikeSwitchSfx, mutedVolume);
		}
		void setPausedGameplayVolume() {
			SetMusicVolume(gameplayMusic, pausedGameplayVolume);
		}
		void setGameplayVolume() {
			SetMusicVolume(gameplayMusic, gameplayMusicVolume);
		}
		void deinit() {
			UnloadMusicStream(gameplayMusic);
			UnloadMusicStream(menuMusic);
			UnloadSound(buttonSfx);
			UnloadSound(victorySfx);
			UnloadSound(spikeHitSfx);
			UnloadSound(spikeSwitchSfx);
		}
	}
}