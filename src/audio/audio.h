#ifndef	AUDIO_H
#define AUDIO_H
#include "raylib.h"

namespace sokoban {
	namespace audio {

		extern Music gameplayMusic;
		extern Music menuMusic;
		extern Sound buttonSfx;
		extern Sound victorySfx;
		extern Sound spikeSwitchSfx;
		extern Sound spikeHitSfx;
		extern bool muted;

		void init();
		void setVolumes();
		void setMutedVolumes();
		void setPausedGameplayVolume();
		void setGameplayVolume();
		void deinit();
	}
}
#endif