#include "hole.h"

namespace sokoban{
	namespace hole {

		void init() {
			hole.sprite = sprites::hole;
			hole.holeArrayPositionI = 10;
			hole.holeArrayPositionJ = 10;
			hole.holeRec.width = static_cast<float>(hole.sprite.width);
			hole.holeRec.height = static_cast<float>(hole.sprite.height);
			hole.holeRec.x = ground::map[hole.holeArrayPositionI]
				[hole.holeArrayPositionJ].rec.x + (ground::map[hole.holeArrayPositionI]
					[hole.holeArrayPositionJ].rec.width / 2) - hole.holeRec.width / 2;
			hole.holeRec.y = ground::map[hole.holeArrayPositionI]
				[hole.holeArrayPositionJ].rec.y + (ground::map[hole.holeArrayPositionI]
					[hole.holeArrayPositionJ].rec.height / 2) - hole.holeRec.height / 2;
		}
		void draw() {
			for (int i = 0; i < ground::maxGroundW; i++) {
				for (int j = 0; j < ground::maxGroundH; j++) {
					if (hole.holeArrayPositionI == i && hole.holeArrayPositionJ == j) {
						#if DEBUG
						DrawRectangleLines(static_cast<int>(hole.holeRec.x),
							static_cast<int>(hole.holeRec.y), static_cast<int>(hole.holeRec.width),
							static_cast<int>(hole.holeRec.height), BLACK);
						#endif
						DrawTexture(hole.sprite, static_cast<int>(hole.holeRec.x),
							static_cast<int>(hole.holeRec.y), WHITE);
					}
				}
			}
		}
		HOLE hole = { 0 };
	}
}