#ifndef	HOLE_H
#define HOLE_H
#include "raylib.h"
#include "ground/ground.h"
#include "sprites/sprites.h"

namespace sokoban {
	namespace hole {

		struct HOLE {
			Rectangle holeRec;
			int holeArrayPositionI;
			int holeArrayPositionJ;
			Texture2D sprite;
		};

		void init();
		void draw();
		extern HOLE hole;
	}
}
#endif