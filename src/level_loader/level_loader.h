#ifndef LEVEL_LOADER_H
#define LEVEL_LOADER_H
#include "raylib.h"
#include "ground/ground.h"
#include "player/player.h"
#include "ball/ball.h"
#include "hole/hole.h"
#include "spikes/spikes.h"

namespace sokoban {
	namespace level_loader {

		const int amountOfLevels = 6;
		const int threeStars = 3;
		const int twoStars = 2;
		const int oneStar = 1;
		const int zeroStars = 0;
		extern int selectedLevel;
		struct LEVEL {
			bool unlocked;
			bool completed;
			int stars;
			int minSteps;
			int medSteps;
			int activeSpikes;
			Vector2 playerArrayPos;
			Vector2 ballArrayPos;
			Vector2 holeArrayPos;
			Vector2 spikesArrayPos[spikes::maxSpikes];
			bool activeGround[ground::maxGroundW][ground::maxGroundH];
		};

		void init();
		void initLevel(int levelToInit);
		void setLevelStars();
		void unlockLevels();
		extern LEVEL levels[amountOfLevels];
	}
}
#endif
