#include "level_loader.h"

namespace sokoban {
	namespace level_loader {

		int selectedLevel = 0;

		void init() {
			levels[0].unlocked = true;            levels[0].completed = false;
			levels[0].activeGround[5][7] = true;  levels[0].activeGround[5][8] = true;
			levels[0].activeGround[6][7] = true;  levels[0].activeGround[6][8] = true;
			levels[0].activeGround[6][9] = true;  levels[0].activeGround[6][10] = true;
			levels[0].activeGround[6][11] = true; levels[0].activeGround[6][12] = true;
			levels[0].activeGround[7][7] = true;  levels[0].activeGround[7][8] = true;
			levels[0].activeGround[7][9] = true;  levels[0].activeGround[8][8] = true;
			levels[0].activeGround[9][8] = true;  levels[0].playerArrayPos = { 6, 12 };
			levels[0].ballArrayPos = { 6, 11 };	  levels[0].holeArrayPos = { 9, 8 };
			levels[0].minSteps = 13;              levels[0].medSteps = 15;
			levels[0].activeSpikes = 0;

			levels[1].unlocked = false;           levels[1].completed = false;
			levels[1].activeGround[0][3] = true;  levels[1].activeGround[0][4] = true;
			levels[1].activeGround[0][5] = true;  levels[1].activeGround[1][3] = true;
			levels[1].activeGround[1][4] = true;  levels[1].activeGround[1][5] = true;
			levels[1].activeGround[1][9] = true;  levels[1].activeGround[1][10] = true;
			levels[1].activeGround[1][11] = true; levels[1].activeGround[2][4] = true;
			levels[1].activeGround[2][5] = true;  levels[1].activeGround[2][6] = true;
			levels[1].activeGround[2][7] = true;  levels[1].activeGround[2][8] = true;
			levels[1].activeGround[2][9] = true;  levels[1].activeGround[2][10] = true;
			levels[1].activeGround[2][11] = true; levels[1].activeGround[3][9] = true;
			levels[1].activeGround[4][9] = true;  levels[1].activeGround[4][10] = true;
			levels[1].activeGround[5][8] = true;  levels[1].activeGround[5][9] = true;
			levels[1].activeGround[5][10] = true; levels[1].activeGround[6][8] = true;
			levels[1].activeGround[6][9] = true;  levels[1].activeGround[6][10] = true;
			levels[1].playerArrayPos = { 0, 5 };  levels[1].ballArrayPos = { 1, 4 };
			levels[1].holeArrayPos = { 6, 10 };   levels[1].minSteps = 29;
			levels[1].medSteps = 31;              levels[1].activeSpikes = 0;

			levels[2].unlocked = false;           levels[2].completed = false;
			levels[2].activeGround[0][9] = true;  levels[2].activeGround[6][9] = true;
			levels[2].activeGround[1][9] = true;  levels[2].activeGround[6][10] = true;
			levels[2].activeGround[2][8] = true;  levels[2].activeGround[6][11] = true;
			levels[2].activeGround[2][9] = true;  levels[2].activeGround[7][10] = true;
			levels[2].activeGround[2][10] = true; levels[2].activeGround[7][11] = true;
			levels[2].activeGround[3][4] = true;  levels[2].activeGround[8][5] = true;
			levels[2].activeGround[3][5] = true;  levels[2].activeGround[8][6] = true;
			levels[2].activeGround[3][6] = true;  levels[2].activeGround[8][7] = true;
			levels[2].activeGround[3][8] = true;  levels[2].activeGround[8][8] = true;
			levels[2].activeGround[3][9] = true;  levels[2].activeGround[8][11] = true;
			levels[2].activeGround[3][10] = true; levels[2].activeGround[9][6] = true;
			levels[2].activeGround[4][4] = true;  levels[2].activeGround[9][7] = true;
			levels[2].activeGround[4][5] = true;  levels[2].activeGround[9][8] = true;
			levels[2].activeGround[4][6] = true;  levels[2].activeGround[9][11] = true;
			levels[2].activeGround[4][7] = true;  levels[2].activeGround[9][12] = true;
			levels[2].activeGround[4][8] = true;  levels[2].activeGround[10][7] = true;
			levels[2].activeGround[4][9] = true;  levels[2].activeGround[10][8] = true;
			levels[2].activeGround[5][5] = true;  levels[2].activeGround[10][9] = true;
			levels[2].activeGround[5][6] = true;  levels[2].activeGround[10][10] = true;
			levels[2].activeGround[5][7] = true;  levels[2].activeGround[10][11] = true;
			levels[2].activeGround[5][8] = true;  levels[2].activeGround[10][12] = true;
			levels[2].activeGround[5][10] = true; levels[2].activeGround[11][7] = true;
			levels[2].activeGround[5][11] = true; levels[2].activeGround[11][8] = true;
			levels[2].activeGround[6][7] = true;  levels[2].activeGround[11][9] = true;
			levels[2].activeGround[6][8] = true;  levels[2].ballArrayPos = { 1, 9 };
			levels[2].playerArrayPos = { 0, 9 };  levels[2].holeArrayPos = { 8, 5 };
			levels[2].minSteps = 36;              levels[2].medSteps = 52;
			levels[2].activeSpikes = 1;           levels[2].spikesArrayPos[0] = { 4, 8 };

			levels[3].unlocked = false;           levels[3].completed = false;
			levels[3].activeGround[1][8] = true;  levels[3].activeGround[6][4] = true;
			levels[3].activeGround[2][8] = true;  levels[3].activeGround[6][10] = true;
			levels[3].activeGround[2][9] = true;  levels[3].activeGround[7][3] = true;
			levels[3].activeGround[2][10] = true; levels[3].activeGround[7][4] = true;
			levels[3].activeGround[3][7] = true;  levels[3].activeGround[7][5] = true;
			levels[3].activeGround[3][8] = true;  levels[3].activeGround[7][10] = true;
			levels[3].activeGround[3][9] = true;  levels[3].activeGround[7][11] = true;
			levels[3].activeGround[3][10] = true; levels[3].activeGround[7][12] = true;
			levels[3].activeGround[4][3] = true;  levels[3].activeGround[8][3] = true;
			levels[3].activeGround[4][4] = true;  levels[3].activeGround[8][4] = true;
			levels[3].activeGround[4][5] = true;  levels[3].activeGround[8][5] = true;
			levels[3].activeGround[4][7] = true;  levels[3].activeGround[8][6] = true;
			levels[3].activeGround[4][8] = true;  levels[3].activeGround[8][7] = true;
			levels[3].activeGround[4][9] = true;  levels[3].activeGround[8][8] = true;
			levels[3].activeGround[4][10] = true; levels[3].activeGround[8][9] = true;
			levels[3].activeGround[4][11] = true; levels[3].activeGround[8][10] = true;
			levels[3].activeGround[5][3] = true;  levels[3].activeGround[8][11] = true;
			levels[3].activeGround[5][4] = true;  levels[3].activeGround[8][12] = true;
			levels[3].activeGround[5][5] = true;  levels[3].activeGround[9][3] = true;
			levels[3].activeGround[5][6] = true;  levels[3].activeGround[9][4] = true;
			levels[3].activeGround[5][7] = true;  levels[3].activeGround[9][9] = true;
			levels[3].activeGround[5][8] = true;  levels[3].activeGround[9][10] = true;
			levels[3].activeGround[5][10] = true; levels[3].activeGround[9][11] = true;
			levels[3].activeGround[5][11] = true; levels[3].ballArrayPos = { 3, 9 };
			levels[3].playerArrayPos = { 1, 8 };  levels[3].holeArrayPos = { 9, 3 };
			levels[3].minSteps = 23;			  levels[3].medSteps = 25;
			levels[3].activeSpikes = 3;           levels[3].spikesArrayPos[0] = { 2, 9 };
			levels[3].spikesArrayPos[1] = { 6, 4 };  levels[3].spikesArrayPos[2] = { 8, 8 };
		
			levels[4].unlocked = false;           levels[4].completed = false;
			levels[4].activeGround[0][2] = true;  levels[4].activeGround[7][9] = true;
			levels[4].activeGround[0][3] = true;  levels[4].activeGround[7][11] = true;
			levels[4].activeGround[0][6] = true;  levels[4].activeGround[8][0] = true;
			levels[4].activeGround[0][7] = true;  levels[4].activeGround[8][1] = true;
			levels[4].activeGround[0][8] = true;  levels[4].activeGround[8][2] = true;
			levels[4].activeGround[0][10] = true; levels[4].activeGround[8][5] = true;
			levels[4].activeGround[0][11] = true; levels[4].activeGround[8][6] = true;
			levels[4].activeGround[1][2] = true;  levels[4].activeGround[8][7] = true;
			levels[4].activeGround[1][3] = true;  levels[4].activeGround[8][8] = true;
			levels[4].activeGround[1][4] = true;  levels[4].activeGround[8][9] = true;
			levels[4].activeGround[1][5] = true;  levels[4].activeGround[8][11] = true;
			levels[4].activeGround[1][6] = true;  levels[4].activeGround[9][0] = true;
			levels[4].activeGround[1][7] = true;  levels[4].activeGround[9][1] = true;
			levels[4].activeGround[1][8] = true;  levels[4].activeGround[9][2] = true;
			levels[4].activeGround[1][9] = true;  levels[4].activeGround[9][3] = true;
			levels[4].activeGround[1][10] = true; levels[4].activeGround[9][7] = true;
			levels[4].activeGround[1][11] = true; levels[4].activeGround[9][10] = true;
			levels[4].activeGround[1][12] = true; levels[4].activeGround[9][11] = true;
			levels[4].activeGround[2][2] = true;  levels[4].activeGround[9][12] = true;
			levels[4].activeGround[2][3] = true;  levels[4].activeGround[10][1] = true;
			levels[4].activeGround[2][7] = true;  levels[4].activeGround[10][2] = true;
			levels[4].activeGround[2][11] = true; levels[4].activeGround[10][3] = true;
			levels[4].activeGround[2][12] = true; levels[4].activeGround[10][4] = true;
			levels[4].activeGround[3][2] = true;  levels[4].activeGround[10][5] = true;
			levels[4].activeGround[3][7] = true;  levels[4].activeGround[10][6] = true;
			levels[4].activeGround[3][11] = true; levels[4].activeGround[10][7] = true;
			levels[4].activeGround[3][12] = true; levels[4].activeGround[10][9] = true;
			levels[4].activeGround[4][2] = true;  levels[4].activeGround[10][10] = true;
			levels[4].activeGround[4][3] = true;  levels[4].activeGround[10][11] = true;
			levels[4].activeGround[4][6] = true;  levels[4].activeGround[10][12] = true;
			levels[4].activeGround[4][7] = true;  levels[4].activeGround[11][2] = true;
			levels[4].activeGround[4][8] = true;  levels[4].activeGround[11][3] = true;
			levels[4].activeGround[4][10] = true; levels[4].activeGround[11][4] = true;
			levels[4].activeGround[4][11] = true; levels[4].activeGround[11][5] = true;
			levels[4].activeGround[4][12] = true; levels[4].activeGround[11][6] = true;
			levels[4].activeGround[5][1] = true;  levels[4].activeGround[11][7] = true;
			levels[4].activeGround[5][2] = true;  levels[4].activeGround[11][8] = true;
			levels[4].activeGround[5][3] = true;  levels[4].activeGround[11][9] = true;
			levels[4].activeGround[5][6] = true;  levels[4].activeGround[11][10] = true;
			levels[4].activeGround[5][7] = true;  levels[4].activeGround[11][11] = true;
			levels[4].activeGround[5][8] = true;  levels[4].activeGround[11][12] = true;
			levels[4].activeGround[6][1] = true;  levels[4].activeGround[12][6] = true;
			levels[4].activeGround[6][2] = true;  levels[4].activeGround[12][7] = true;
			levels[4].activeGround[6][3] = true;  levels[4].activeGround[12][8] = true;
			levels[4].activeGround[6][6] = true;  levels[4].activeGround[12][10] = true;
			levels[4].activeGround[6][11] = true; levels[4].activeGround[12][11] = true;
			levels[4].activeGround[7][1] = true;  levels[4].activeGround[12][12] = true;
			levels[4].activeGround[7][5] = true;  levels[4].activeGround[5][10] = true;
			levels[4].activeGround[7][6] = true;  levels[4].activeGround[5][11] = true;
			levels[4].activeGround[7][8] = true;  levels[4].activeGround[5][12] = true;
			levels[4].playerArrayPos = { 0, 7 };  levels[4].ballArrayPos = { 1, 7 };
			levels[4].holeArrayPos = { 12, 7 };   levels[4].minSteps = 34;
			levels[4].medSteps = 44;              levels[4].activeSpikes = 4;
			levels[4].spikesArrayPos[0] = { 4, 11 };  levels[4].spikesArrayPos[1] = { 5, 11 };
			levels[4].spikesArrayPos[2] = { 11, 9 };  levels[4].spikesArrayPos[3] = { 11, 10 };

			levels[5].unlocked = false;           levels[5].completed = false;
			levels[5].activeGround[0][0] = true;  levels[5].activeGround[6][3] = true;
			levels[5].activeGround[0][1] = true;  levels[5].activeGround[6][4] = true;
			levels[5].activeGround[0][2] = true;  levels[5].activeGround[6][5] = true;
			levels[5].activeGround[0][9] = true;  levels[5].activeGround[6][6] = true;
			levels[5].activeGround[0][10] = true; levels[5].activeGround[6][7] = true;
			levels[5].activeGround[0][11] = true; levels[5].activeGround[6][8] = true;
			levels[5].activeGround[1][0] = true;  levels[5].activeGround[6][9] = true;
			levels[5].activeGround[1][2] = true;  levels[5].activeGround[6][10] = true;
			levels[5].activeGround[1][9] = true;  levels[5].activeGround[6][11] = true;
			levels[5].activeGround[1][11] = true; levels[5].activeGround[6][12] = true;
			levels[5].activeGround[2][0] = true;  levels[5].activeGround[7][6] = true;
			levels[5].activeGround[2][1] = true;  levels[5].activeGround[7][7] = true;
			levels[5].activeGround[2][2] = true;  levels[5].activeGround[7][9] = true;
			levels[5].activeGround[2][3] = true;  levels[5].activeGround[7][11] = true;
			levels[5].activeGround[2][6] = true;  levels[5].activeGround[7][12] = true;
			levels[5].activeGround[2][7] = true;  levels[5].activeGround[8][7] = true;
			levels[5].activeGround[2][9] = true;  levels[5].activeGround[8][9] = true;
			levels[5].activeGround[2][10] = true; levels[5].activeGround[8][11] = true;
			levels[5].activeGround[2][11] = true; levels[5].activeGround[9][3] = true;
			levels[5].activeGround[3][0] = true;  levels[5].activeGround[9][4] = true;
			levels[5].activeGround[3][2] = true;  levels[5].activeGround[9][5] = true;
			levels[5].activeGround[3][3] = true;  levels[5].activeGround[9][6] = true;
			levels[5].activeGround[3][4] = true;  levels[5].activeGround[9][7] = true;
			levels[5].activeGround[3][6] = true;  levels[5].activeGround[9][8] = true;
			levels[5].activeGround[3][7] = true;  levels[5].activeGround[9][9] = true;
			levels[5].activeGround[3][8] = true;  levels[5].activeGround[9][10] = true;
			levels[5].activeGround[3][9] = true;  levels[5].activeGround[9][11] = true;
			levels[5].activeGround[3][10] = true; levels[5].activeGround[10][3] = true;
			levels[5].activeGround[4][2] = true;  levels[5].activeGround[10][5] = true;
			levels[5].activeGround[4][3] = true;  levels[5].activeGround[10][7] = true;
			levels[5].activeGround[4][4] = true;  levels[5].activeGround[10][10] = true;
			levels[5].activeGround[4][5] = true;  levels[5].activeGround[10][11] = true;
			levels[5].activeGround[4][6] = true;  levels[5].activeGround[11][3] = true;
			levels[5].activeGround[4][7] = true;  levels[5].activeGround[11][4] = true;
			levels[5].activeGround[4][9] = true;  levels[5].activeGround[11][5] = true;
			levels[5].activeGround[5][3] = true;  levels[5].activeGround[11][6] = true;
			levels[5].activeGround[5][6] = true;  levels[5].activeGround[11][7] = true;
			levels[5].activeGround[5][9] = true;  levels[5].activeGround[12][5] = true;
			levels[5].playerArrayPos = { 12, 5 }; levels[5].ballArrayPos = { 11, 5 };
			levels[5].holeArrayPos = { 3, 0 };    levels[5].minSteps = 70;
			levels[5].medSteps = 72;              levels[5].activeSpikes = 6;
			levels[5].spikesArrayPos[0] = { 3, 2 };  levels[5].spikesArrayPos[1] = { 3, 6 };
			levels[5].spikesArrayPos[2] = { 3, 9 };  levels[5].spikesArrayPos[3] = { 6, 6 };
			levels[5].spikesArrayPos[4] = { 6, 11 }; levels[5].spikesArrayPos[5] = { 9, 7 };

			for (int i = 0; i < amountOfLevels; i++) {
				levels[i].stars = 0;
				#if DEBUG
				levels[i].unlocked = true;   
				levels[i].completed = true;
				#endif
			}
		}
		void initLevel(int levelToInit) {
			player::sokobanPlayer.steps = 0;
			player::sokobanPlayer.arrayPositionI = static_cast<int>(levels[levelToInit].playerArrayPos.x);
			player::sokobanPlayer.arrayPositionJ = static_cast<int>(levels[levelToInit].playerArrayPos.y);
			player::sokobanPlayer.rec.x = ground::map[player::sokobanPlayer.arrayPositionI]
				[player::sokobanPlayer.arrayPositionJ].rec.x + (ground::map[player::sokobanPlayer.arrayPositionI]
					[player::sokobanPlayer.arrayPositionJ].rec.width / 2) - player::sokobanPlayer.rec.width / 2;
			player::sokobanPlayer.rec.y = ground::map[player::sokobanPlayer.arrayPositionI]
				[player::sokobanPlayer.arrayPositionJ].rec.y + (ground::map[player::sokobanPlayer.arrayPositionI]
					[player::sokobanPlayer.arrayPositionJ].rec.height / 2) - player::sokobanPlayer.rec.height / 2;
			ball::ball.inHole = false;
			ball::ball.arrayPositionI = static_cast<int>(levels[levelToInit].ballArrayPos.x);
			ball::ball.arrayPositionJ = static_cast<int>(levels[levelToInit].ballArrayPos.y);
			hole::hole.holeArrayPositionI = static_cast<int>(levels[levelToInit].holeArrayPos.x);
			hole::hole.holeArrayPositionJ = static_cast<int>(levels[levelToInit].holeArrayPos.y);
			ball::ball.rec.x = ground::map[ball::ball.arrayPositionI]
				[ball::ball.arrayPositionJ].rec.x + (ground::map[ball::ball.arrayPositionI]
					[ball::ball.arrayPositionJ].rec.width / 2) - ball::ball.rec.width / 2;
			ball::ball.rec.y = ground::map[ball::ball.arrayPositionI]
				[ball::ball.arrayPositionJ].rec.y + (ground::map[ball::ball.arrayPositionI]
					[ball::ball.arrayPositionJ].rec.height / 2) - ball::ball.rec.height / 2;
			hole::hole.holeRec.x = ground::map[hole::hole.holeArrayPositionI]
				[hole::hole.holeArrayPositionJ].rec.x + (ground::map[hole::hole.holeArrayPositionI]
					[hole::hole.holeArrayPositionJ].rec.width / 2) - hole::hole.holeRec.width / 2;
			hole::hole.holeRec.y = ground::map[hole::hole.holeArrayPositionI]
				[hole::hole.holeArrayPositionJ].rec.y + (ground::map[hole::hole.holeArrayPositionI]
					[hole::hole.holeArrayPositionJ].rec.height / 2) - hole::hole.holeRec.height / 2;
			for (int i = 0; i < ground::maxGroundW; i++){
				for (int j = 0; j < ground::maxGroundH; j++) {
					if (levels[levelToInit].activeGround[i][j]==true) {
						ground::map[i][j].active = true;
					}
					else {
						ground::map[i][j].active = false;
					}
				}
			}
			spikes::reinitCounter();
			spikes::activeSpikes = levels[selectedLevel].activeSpikes;
			for (int i = 0; i < spikes::maxSpikes; i++) {
				if (i < levels[selectedLevel].activeSpikes) {
					spikes::spike[i].inGame = true;
					spikes::spike[i].arrayPositionI = static_cast<int>(levels[selectedLevel].spikesArrayPos[i].x);
					spikes::spike[i].arrayPositionJ = static_cast<int>(levels[selectedLevel].spikesArrayPos[i].y);
					spikes::spike[i].rec.x = ground::map[spikes::spike[i].arrayPositionI]
					[spikes::spike[i].arrayPositionJ].rec.x + (ground::map[spikes::spike[i].arrayPositionI]
						[spikes::spike[i].arrayPositionJ].rec.width / 2) - spikes::spike[i].rec.width / 2;
					spikes::spike[i].rec.y = ground::map[spikes::spike[i].arrayPositionI]
						[spikes::spike[i].arrayPositionJ].rec.y + (ground::map[spikes::spike[i].arrayPositionI]
							[spikes::spike[i].arrayPositionJ].rec.height / 2) - (spikes::spike[i].rec.height * 0.75f);
				}
				else {
					spikes::spike[i].inGame = false;
				}
			}
		}
		void setLevelStars() {			
			if (player::sokobanPlayer.steps<levels[selectedLevel].minSteps) {
				player::sokobanPlayer.obtainedStars = threeStars;
			}
			else if (player::sokobanPlayer.steps<levels[selectedLevel].medSteps){
				player::sokobanPlayer.obtainedStars = twoStars;
			}
			else {
				player::sokobanPlayer.obtainedStars = oneStar;
			}
			if (player::sokobanPlayer.obtainedStars > levels[selectedLevel].stars){
				levels[selectedLevel].stars = player::sokobanPlayer.obtainedStars;
			}
		}
		void unlockLevels() {
			if (!levels[selectedLevel].completed) {
				levels[selectedLevel].completed = true;
			}
			if (selectedLevel < amountOfLevels - 1 && !levels[selectedLevel + 1].unlocked) {
				levels[selectedLevel + 1].unlocked = true;
			}
		}
		LEVEL levels[amountOfLevels] = { 0 };
	}
}