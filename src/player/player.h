#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
#include "ground/ground.h"
#include "screens_manager/screens.h"
#include "ball/ball.h"
#include "hole/hole.h"
#include "sprites/sprites.h"

namespace sokoban {
	namespace player {

		struct PLAYER {
			int steps;
			bool alive;
			Rectangle rec;
			int arrayPositionI;
			int arrayPositionJ;
			int obtainedStars;
			Texture2D sprite;
		};
	
		void init();
		void inputs();
		void update();
		void draw();
		extern PLAYER sokobanPlayer;
	}
}
#endif
