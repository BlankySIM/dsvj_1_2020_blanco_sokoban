#include "player.h"

namespace sokoban {
	namespace player {
		
		int key;
		const int minusc = 96;
		const int mayusc = 32;

		static void move(int newPlayerI, int newPlayerJ, int newBallI, int newBallJ) {
			if (newPlayerI == ball::ball.arrayPositionI && newPlayerJ == ball::ball.arrayPositionJ &&
				ground::map[newBallI][newBallJ].active && newBallI >= 0 && newBallI < ground::maxGroundW &&
				newBallJ >= 0 && newBallJ < ground::maxGroundH) {
				sokobanPlayer.arrayPositionI = newPlayerI;
				sokobanPlayer.arrayPositionJ = newPlayerJ;
				sokobanPlayer.rec.x = ground::map[sokobanPlayer.arrayPositionI]
					[sokobanPlayer.arrayPositionJ].rec.x + (ground::map[sokobanPlayer.arrayPositionI]
						[sokobanPlayer.arrayPositionJ].rec.width / 2) - sokobanPlayer.rec.width / 2;
				sokobanPlayer.rec.y = ground::map[sokobanPlayer.arrayPositionI]
					[sokobanPlayer.arrayPositionJ].rec.y + (ground::map[sokobanPlayer.arrayPositionI]
						[sokobanPlayer.arrayPositionJ].rec.height / 2) - sokobanPlayer.rec.height / 2;
				ball::move(newBallI, newBallJ);
				if (sokobanPlayer.steps < 100000) {
					sokobanPlayer.steps++;
				}
			}
			else if (ground::map[newPlayerI][newPlayerJ].active &&
				(newPlayerI != ball::ball.arrayPositionI || newPlayerJ != ball::ball.arrayPositionJ) &&
				(newPlayerI != hole::hole.holeArrayPositionI || newPlayerJ != hole::hole.holeArrayPositionJ) &&
				newPlayerI >= 0 && newPlayerI < ground::maxGroundW &&
				newPlayerJ >= 0 && newPlayerJ < ground::maxGroundH) {
				sokobanPlayer.arrayPositionI = newPlayerI;
				sokobanPlayer.arrayPositionJ = newPlayerJ;
				sokobanPlayer.rec.x = ground::map[sokobanPlayer.arrayPositionI]
					[sokobanPlayer.arrayPositionJ].rec.x + (ground::map[sokobanPlayer.arrayPositionI]
						[sokobanPlayer.arrayPositionJ].rec.width / 2) - sokobanPlayer.rec.width / 2;
				sokobanPlayer.rec.y = ground::map[sokobanPlayer.arrayPositionI]
					[sokobanPlayer.arrayPositionJ].rec.y + (ground::map[sokobanPlayer.arrayPositionI]
						[sokobanPlayer.arrayPositionJ].rec.height / 2) - sokobanPlayer.rec.height / 2;
				if (sokobanPlayer.steps < 100000) {
					sokobanPlayer.steps++;
				}
			}
		}
		void init() {
			sokobanPlayer.sprite = sprites::playerDown;
			sokobanPlayer.steps = 0;
			sokobanPlayer.obtainedStars = 0;
			sokobanPlayer.alive = true;
			sokobanPlayer.arrayPositionI = 2;
			sokobanPlayer.arrayPositionJ = 2;
			sokobanPlayer.rec.width = static_cast<float>(sokobanPlayer.sprite.width);
			sokobanPlayer.rec.height = static_cast<float>(sokobanPlayer.sprite.height);
			sokobanPlayer.rec.x = ground::map[sokobanPlayer.arrayPositionI]
				[sokobanPlayer.arrayPositionJ].rec.x + (ground::map[sokobanPlayer.arrayPositionI]
				[sokobanPlayer.arrayPositionJ].rec.width/2) - sokobanPlayer.rec.width/2;
			sokobanPlayer.rec.y = ground::map[sokobanPlayer.arrayPositionI]
				[sokobanPlayer.arrayPositionJ].rec.y + (ground::map[sokobanPlayer.arrayPositionI]
					[sokobanPlayer.arrayPositionJ].rec.height / 2) - sokobanPlayer.rec.height/2;
		}
		void inputs() {
			key = GetKeyPressed();
			if (key > minusc){
				key -= mayusc;
			}
		}
		void update() {
			switch (key) {
			case KEY_W:
				move(sokobanPlayer.arrayPositionI, sokobanPlayer.arrayPositionJ - 1,
					ball::ball.arrayPositionI, ball::ball.arrayPositionJ - 1);
				sokobanPlayer.sprite = sprites::playerUp;
				break;
			case KEY_S:
				move(sokobanPlayer.arrayPositionI, sokobanPlayer.arrayPositionJ + 1,
					ball::ball.arrayPositionI, ball::ball.arrayPositionJ + 1);
				sokobanPlayer.sprite = sprites::playerDown;
				break;
			case KEY_A:
				move(sokobanPlayer.arrayPositionI - 1, sokobanPlayer.arrayPositionJ,
					ball::ball.arrayPositionI - 1, ball::ball.arrayPositionJ);
				sokobanPlayer.sprite = sprites::playerLeft;
				break;
			case KEY_D:
				move(sokobanPlayer.arrayPositionI + 1, sokobanPlayer.arrayPositionJ,
					ball::ball.arrayPositionI + 1, ball::ball.arrayPositionJ);
				sokobanPlayer.sprite = sprites::playerRight;
				break;
			}
		}
		void draw() {
			for (int i = 0; i < ground::maxGroundW; i++) {
				for (int j = 0; j < ground::maxGroundH; j++) {
					if (sokobanPlayer.arrayPositionI==i && sokobanPlayer.arrayPositionJ==j) {
						#if DEBUG
						DrawRectangle(static_cast<int>(sokobanPlayer.rec.x), static_cast<int>(sokobanPlayer.rec.y),
							static_cast<int>(sokobanPlayer.rec.width), static_cast<int>(sokobanPlayer.rec.height),
							Fade(WHITE, 0.4f));
						#endif
						DrawTexture(sokobanPlayer.sprite, static_cast<int>(sokobanPlayer.rec.x),
							static_cast<int>(sokobanPlayer.rec.y), WHITE);
					}
				}
			}
		}
		PLAYER sokobanPlayer = { 0 };
	}
}