#include "ball.h"

namespace sokoban {
	namespace ball {

		void init() {
			ball.sprite = sprites::ball;
			ball.inHole = false;
			ball.arrayPositionI = 5;
			ball.arrayPositionJ = 9;
			ball.rec.width = static_cast<float>(ball.sprite.width);
			ball.rec.height = static_cast<float>(ball.sprite.height);
			ball.rec.x = ground::map[ball.arrayPositionI]
				[ball.arrayPositionJ].rec.x + (ground::map[ball.arrayPositionI]
					[ball.arrayPositionJ].rec.width / 2) - ball.rec.width / 2;
			ball.rec.y = ground::map[ball.arrayPositionI]
				[ball.arrayPositionJ].rec.y + (ground::map[ball.arrayPositionI]
					[ball.arrayPositionJ].rec.height / 2) - ball.rec.height / 2;
		}
		void move(int newBallI, int newBallJ) {
			ball.arrayPositionI = newBallI;
			ball.arrayPositionJ = newBallJ;
			ball.rec.x = ground::map[ball.arrayPositionI]
				[ball.arrayPositionJ].rec.x + (ground::map[ball.arrayPositionI]
					[ball.arrayPositionJ].rec.width / 2) - ball.rec.width / 2;
			ball.rec.y = ground::map[ball.arrayPositionI]
				[ball.arrayPositionJ].rec.y + (ground::map[ball.arrayPositionI]
					[ball.arrayPositionJ].rec.height / 2) - ball.rec.height / 2;
		}
		void update() {
			if (ball.arrayPositionI == hole::hole.holeArrayPositionI
				&& ball.arrayPositionJ == hole::hole.holeArrayPositionJ) {
				ball.inHole = true;
			}
		}
		void draw() {
			for (int i = 0; i < ground::maxGroundW; i++) {
				for (int j = 0; j < ground::maxGroundH; j++) {
					if (ball.arrayPositionI == i && ball.arrayPositionJ == j) {
						#if DEBUG
						DrawCircle(static_cast<int>(ball.rec.x + ball.rec.width/2),
							static_cast<int>(ball.rec.y + ball.rec.height/2), ball.rec.width / 3, Fade(BLUE, 0.9f));
						DrawRectangleLines(static_cast<int>(ball.rec.x),
							static_cast<int>(ball.rec.y), static_cast<int>(ball.rec.width),
							static_cast<int>(ball.rec.height), BLUE);
						#endif
						DrawTexture(ball.sprite, static_cast<int>(ball.rec.x),
							static_cast<int>(ball.rec.y), WHITE);
					}
				}
			}
		}
		BALL ball = { 0 };
	}
}