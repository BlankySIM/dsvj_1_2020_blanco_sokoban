#ifndef BALL_H
#define BALL_H
#include "raylib.h"
#include "screens_manager/screens.h"
#include "ground/ground.h"
#include "hole/hole.h"
#include "sprites/sprites.h"

namespace sokoban {
	namespace ball {

		struct BALL{
			bool inHole;
			Rectangle rec;
			int arrayPositionI;
			int arrayPositionJ;
			Texture2D sprite;
		};
		
		void init();
		void move(int newArrayI, int newArrayJ);
		void update();
		void draw();
		extern BALL ball;
	}
}
#endif
