#ifndef SPIKES_H
#define SPIKES_H
#include "raylib.h"
#include "player/player.h"
#include "ground/ground.h"
#include "screens_manager/screens.h"
#include "sprites/sprites.h"
#include "audio/audio.h"

namespace sokoban {
	namespace spikes {

		const int maxSpikes = 6;
		extern int activeSpikes;
		struct SPIKES {
			bool inGame;
			Rectangle rec;
			int arrayPositionI;
			int arrayPositionJ;
			bool active;
			Texture2D inactiveSprite;
			Texture2D activeSprite;
		};

		void init();
		void update();
		void reinitCounter();
		void draw();
		extern SPIKES spike[maxSpikes];
	}
}
#endif