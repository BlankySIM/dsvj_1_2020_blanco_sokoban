#include "spikes.h"

namespace sokoban {
	namespace spikes {

		const float maxCounter = 50.0f;
		float counter = maxCounter;
		const float counterModifier = 50.0f;
		int activeSpikes = 0;

		static void activeTimer() {
			if (activeSpikes>0) {
				counter -= counterModifier * GetFrameTime();
				if (counter <= 0) {
					PlaySound(audio::spikeSwitchSfx);
					counter = maxCounter;
					for (int i = 0; i < maxSpikes; i++) {
						spike[i].active = !spike[i].active;
					}
				}
			}
		}
		void init() {
			for (int i = 0; i < maxSpikes; i++) {
				spike[i].inactiveSprite = sprites::spikes;
				spike[i].activeSprite = sprites::activeSpikes;
				spike[i].active = true;
				spike[i].arrayPositionI = 0;
				spike[i].arrayPositionJ = 0;
				spike[i].rec.width = static_cast<float>(spike[i].activeSprite.width);
				spike[i].rec.height = static_cast<float>(spike[i].activeSprite.height);
				spike[i].rec.x = ground::map[spike[i].arrayPositionI]
					[spike[i].arrayPositionJ].rec.x + (ground::map[spike[i].arrayPositionI]
						[spike[i].arrayPositionJ].rec.width / 2) - spike[i].rec.width / 2;
				spike[i].rec.y = ground::map[spike[i].arrayPositionI]
					[spike[i].arrayPositionJ].rec.y + (ground::map[spike[i].arrayPositionI]
						[spike[i].arrayPositionJ].rec.height / 2) - (spike[i].rec.height * 0.75f);
			}
		}
		void update() {
			activeTimer();
			for (int i = 0; i < maxSpikes; i++) {
				if (spike[i].active && spike[i].inGame &&
					player::sokobanPlayer.arrayPositionI == spike[i].arrayPositionI &&
					player::sokobanPlayer.arrayPositionJ == spike[i].arrayPositionJ) {
					player::sokobanPlayer.alive = false;
					PlaySound(audio::spikeHitSfx);
					reinitCounter();
				}
			}
		}
		void reinitCounter() {
			counter = maxCounter;
			for (int i = 0; i < maxSpikes; i++) {
				spike[i].active = true;
			}
		}
		void draw() {
			for (int i = 0; i < ground::maxGroundW; i++) {
				for (int j = 0; j < ground::maxGroundH; j++) {
					for (int x = 0; x < maxSpikes; x++) {
						if (spike[x].arrayPositionI == i && spike[x].arrayPositionJ == j) {
							if (spike[x].inGame) {
								if (spike[x].active) {
									#if DEBUG
									DrawRectangle(static_cast<int>(spike[x].rec.x), static_cast<int>(spike[x].rec.y),
										static_cast<int>(spike[x].rec.width), static_cast<int>(spike[x].rec.height), Fade(RED, 0.6f));
									#endif
									DrawTexture(spike[x].activeSprite, static_cast<int>(spike[x].rec.x),
										static_cast<int>(spike[x].rec.y), WHITE);
								}
								else {
									#if DEBUG
									DrawRectangle(static_cast<int>(spike[x].rec.x), static_cast<int>(spike[x].rec.y),
										static_cast<int>(spike[x].rec.width), static_cast<int>(spike[x].rec.height), 
										Fade(ground::map[i][j].color, 0.6f));
									#endif
									DrawTexture(spike[x].inactiveSprite, static_cast<int>(spike[x].rec.x),
										static_cast<int>(spike[x].rec.y), WHITE);
								}
							}
						}
					}
				}
			}
		}
		SPIKES spike[maxSpikes] = { 0 };
	}
}